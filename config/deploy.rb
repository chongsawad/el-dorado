load 'deploy' if respond_to?(:namespace) # cap2 differentiator

set :repository, 'git@github.com:Chongsawad/El-Dorado.git'
set :ssh_options, {:forward_agent => true}
set :domain, "inice@inice"
set :scm, :git
set :password, 'kckctv9t2925'

set :user, ENV['USER'] || 'inice'
set :application, ENV['APPNAME'] || 'eldorado'
set :deploy_to, "/home/inice/InstantSOA/deploy_cap/#{user}/#{application}"

role :app, domain 
role :web, domain 
role :db,  domain , :primary => true

after   'deploy:setup', 'deploy:restart'
after   'deploy:restart', 'database:config'
after	'database:config', 'database:create'
after	'database:create', 'database:setup'

namespace :database do

  task :config do
    puts "------------- Generate Databaseyml ---------------"
    db_config = ERB.new <<-EOF
base: &base
  adapter: mysql
  encoding: utf8
  reconnect: false
  pool: 5
  username: root
  password:
  host: localhost

development:
  database: #{application}_development
  <<: *base

test:
  database: #{application}_test
  <<: *base

production:
  database: #{application}_production
  <<: *base
EOF
    run "mkdir -p #{current_path}/config/"
    put db_config.result, "#{current_path}/config/database.yml"
  end

  task :create do
    puts "------------- Create Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_path}; #{rake} RAILS_ENV=#{rails_env} db:create"
  end

  task :drop do
    puts "------------- Drop Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} db:drop"
  end


  task :setup do
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")

    puts "------------- SCHEMA LOAD ---------------"
    run "cd #{current_path}; #{rake} RAILS_ENV=#{rails_env} db:schema:load"
  end

end

namespace :deploy do
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
    run "mkdir -p #{shared_path}/log"
    run "mkdir -p #{shared_path}/pids"
    run "mkdir -p #{shared_path}/system"
  end
end
